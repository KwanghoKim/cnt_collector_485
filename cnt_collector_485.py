import os, time, json
import serial
import serial.rs485
from pymodbus.client import ModbusSerialClient as ModbusClient
import paho.mqtt.client as mqtt
import struct
import argparse

if os.system("lsmod | grep max7301") == 0:
    os.system("rmmod gpio-max7301")
    os.system("rmmod gpio-max730x")
    os.system("rmmod gpio-pisosr")
    os.system("insmod /lib/modules/5.4.31/extra/max3109test.ko")

# For PION-D3W-035(Power Controller) No.1, UT35A(Temperature Controller) No.1
_serial_port_A = serial.Serial('/dev/ttyMAX1', 38400, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout = 1, rtscts=True)
_serial_port_A.rs485_mode = serial.rs485.RS485Settings()
_serial_port_A.close()
del _serial_port_A

client1 = ModbusClient(method='rtu', port='/dev/ttyMAX1', baudrate=38400,
                      parity='N', stopbits=1, bytesize=8, timeout=1)


# For PION-D3W-035(Power Controller) No.2, UT35A(Temperature Controller) No.1
_serial_port_B = serial.Serial('/dev/ttyMAX0', 38400, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout = 1, rtscts=True)
_serial_port_B.rs485_mode = serial.rs485.RS485Settings()
_serial_port_B.close()
del _serial_port_B

client2 = ModbusClient(method='rtu', port='/dev/ttyMAX0', baudrate=38400,
                      parity='N', stopbits=1, bytesize=8, timeout=1)

# For PION-D3W-035(Power Controller) No.3, UT35A(Temperature Controller) No.1
_serial_port_C = serial.Serial('/dev/ttyMAX2', 38400, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout = 1, rtscts=True)
_serial_port_C.rs485_mode = serial.rs485.RS485Settings()
_serial_port_C.close()
del _serial_port_C

client3 = ModbusClient(method='rtu', port='/dev/ttyMAX2', baudrate=38400,
                      parity='N', stopbits=1, bytesize=8, timeout=1)

# For FIX800(Gas Detector), CWT-MB307C(Flow Pump)
_serial_port_D = serial.Serial('/dev/ttyMAX3', 9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout = 0.3, rtscts=True)
_serial_port_D.rs485_mode = serial.rs485.RS485Settings()
# _serial_port_A.close()
# del _serial_port_A

client4 = ModbusClient(method='rtu', port='/dev/ttyMAX3', baudrate=9600,
                      parity='E', stopbits=1, bytesize=8, timeout=0.5)

client1.connect()
client2.connect()
client3.connect()
client4.connect()

parser = argparse.ArgumentParser()
parser.add_argument('-config',help='')
args = parser.parse_args()

ROOT_PATH = f'/usr/local/sdt/app/{args.config}'

with open(f'{ROOT_PATH}/connect_info.json') as f:
    info = json.load(f)

MQTT_TOPIC = info['mqtt']['topic']
# MQTT_TOPIC = "/axrcnt/device-test/S0NQ0R423090004"
MQTT_ID = info['mqtt']['id']
MQTT_PW = info['mqtt']['pw']
MQTT_HOST_IP = info['mqtt']['host_ip']
MQTT_PORT = info['mqtt']['port']

mqtt_client = mqtt.Client(info['mqtt']['assetCode'])
mqtt_client.username_pw_set(MQTT_ID, MQTT_PW)

def Read_Temperature(client, ch_num):
    if ch_num == 1:
        id = 1
        name = 'tempFn1'
    elif ch_num == 2:
        id = 2
        name = 'tempFn2'
    else: # ch_num == 3
        id = 3
        name = 'tempFn3'
        
    result = client.read_holding_registers(address=2003, count=1, slave=id)
    if not result.isError():
        # print(f'Temperature: {ch_num} {result.registers}')
        value = (result.registers[0])/10
    else:
        print(f'Temperature Err: {ch_num} \n {result}')

    data = {
        "accetCode": info['mqtt']['assetCode'],
        "timestamp": int(time.time()),
        "datatype": "DATA",
        "data": {
            name: value
        }
    }

    Publish_mqtt(data)

def Set_Temperature(client, ch_num, value):
    if ch_num == 1:
        id = 1
    elif ch_num == 2:
        id = 2
    else: # ch_num == 3
        id = 3

    return 0 

def Read_Power(client, ch_num):
    if ch_num == 1:
        id = 11
        name = 'enerWn1'
    elif ch_num == 2:
        id = 12
        name = 'enerWn2'
    else: # ch_num == 3
        id = 13
        name = 'enerWn3'

    r_high = client.read_input_registers(address=325, count=1, slave=id)
    r_low = client.read_input_registers(address=326, count=1, slave=id)
    s_high = client.read_input_registers(address=327, count=1, slave=id)
    s_low = client.read_input_registers(address=328, count=1, slave=id)
    t_high = client.read_input_registers(address=329, count=1, slave=id)
    t_low = client.read_input_registers(address=330, count=1, slave=id)
    # print(f'r_high: {r_high.registers[0]} {r_high.registers[1]}, r_low: {r_low.registers[0]} {r_low.registers[1]}')
    # print(f's_high: {s_high.registers[0]}, s_low: {s_low.registers[0]}')
    # print(f't_high: {t_high.registers[0]}, t_low: {t_low.registers[0]}')
    value = (
        (r_high.registers[0] << 16) + r_low.registers[0] +
        (s_high.registers[0] << 16) + s_low.registers[0] +
        (t_high.registers[0] << 16) + t_low.registers[0]
    )
    
    data = {
        "assetCode": info['mqtt']['assetCode'],
        "timestamp": int(time.time()),
        "dataType": "DATA",
        "data": {
            name: value
        }
    }
    # print(data)
    Publish_mqtt(data)

result = []
def Read_GasDetector(client, ch_num):
    global result
    if ch_num == 1:
        oxygen = '1503024e0002a6b0'
        hydrogen = '1503034e0002a74c'
        name_1 = 'oxygenOn'
        name_2 = 'hydrogenOn'
    else: # ch_num == 2
        id = 22
        oxygen = '1603024e0002a683'
        hydrogen = '1603034e0002a77f'
        name_1 = 'oxygenSb'
        name_2 = 'hydrogenSb'
    
    oxygen_value, hydrogen_value = 0, 0
    
    send_data = bytes.fromhex(oxygen)
    isRun = True
    while isRun:
        client.write(send_data)
        result += client.readline()
        if  len(result) >= 9:
            if result[0] == 21 and result[1] == 3 and result[2] == 4:
                combined_bytes = (result[5] << 24) + (result[6] << 16) + (result[3] << 8) + result[4]
                oxygen_value = struct.unpack('f', struct.pack('I', combined_bytes))[0]

                result.clear()
                client.reset_input_buffer()
                isRun = False
            else:
                while True:
                    result.pop(0)
                    if result[0] == 21:
                        break

    send_data = bytes.fromhex(hydrogen)
    isRun = True
    while isRun:
        client.write(send_data)
        result += client.readline()
        if len(result) >= 9:
            if result[0] == 21 and result[1] == 3 and result[2] == 4:
                combined_bytes = (result[5] << 24) + (result[6] << 16) + (result[3] << 8) + result[4]
                hydrogen_value = struct.unpack('f', struct.pack('I', combined_bytes))[0]

                result.clear()
                client.reset_input_buffer()
                isRun = False
            else:
                while True:
                    result.pop(0)
                    if result[0] == 21:
                        break

    data = {
        "assetCode": info['mqtt']['assetCode'],
        "timestamp": int(time.time()),
        "dataType": "DATA",
        "data": {
            name_1: oxygen_value,
            name_2: hydrogen_value
        }
    }
    # print(data)
    Publish_mqtt(data)

def Set_FlowPump(client, value):
    id = 31

    register_value = int(((value * 0.32) + 4) * 2500)

    client.write_registers(address=40004, values=register_value, slave=id)

def Publish_mqtt(data):
    send_mqtt_data = json.dumps(data, indent=4)

    while True:
        try:
            mqtt_client.connect(host=MQTT_HOST_IP, port=MQTT_PORT)
            break
        except:
            continue
        
    mqtt_client.publish(topic=MQTT_TOPIC, payload=send_mqtt_data)

    while True:
        try:
            mqtt_client.disconnect()
            break
        except:
            continue

def Command_Read():
    with open(f'{ROOT_PATH}/control.json') as f:
        cmd = json.load(f)

    if cmd['type'] != 'null':
        if 'start' in cmd['type'] or 'change' in cmd['type']:
            if cmd['cmd']['precAc'] != 'null':
                Set_FlowPump(client=client4, value=float(cmd['cmd']['precAc']))
            
            if cmd['cmd']['tempFn1'] != 'null':
                Set_Temperature(client=client1, ch_num=1, value=float(cmd['cmd']['tempFn1']))
            
            if cmd['cmd']['tempFn2'] != 'null':
                Set_Temperature(client=client2, ch_num=2, value=float(cmd['cmd']['tempFn1']))
            
            if cmd['cmd']['tempFn3'] != 'null':
                Set_Temperature(client=client2, ch_num=3, value=float(cmd['cmd']['tempFn1']))
        else: # 'stop' in cmd['type]
            Set_FlowPump(client=client4, value=0.0)

    cmd = {
        "type": "null",
        "cmd": {
            "tempFn1": "null",
            "tempFn2": "null",
            "tempFn3": "null",
            "precAc": "null"
        }
    }
    with open(f'{ROOT_PATH}/control.json', 'w') as f:
        json.dump(cmd, f)

while True:
    start = time.time()
    # Need to add a function call for reading control.json
    Command_Read()

    # # Read Temperature
    Read_Temperature(client1, 1)
    Read_Temperature(client2, 2)
    Read_Temperature(client3, 3)

    # # Read Power
    Read_Power(client1, 1)
    Read_Power(client2, 2)
    Read_Power(client3, 3)
    
    # Read GasDetector
    Read_GasDetector(_serial_port_D, 1)
    # Read_GasDetector(_serial_port_D, 2)

    end = time.time()
    diff = end - start
    # print(f'diff: {diff}')

    if diff < 3:
        time.sleep(3.0 - diff)