#!/bin/bash

set -e

sudo sed -i "s/appName/$1/g" /usr/local/sdt/app/$1/cnt_collector_485.service
sudo cp /usr/local/sdt/app/$1/cnt_collector_485.service /etc/systemd/system/$1.service
sudo systemctl start $1
sudo systemctl enable $1

